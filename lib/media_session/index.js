var redis = require("redis"),
    EventEmitter = require("eventemitter3").EventEmitter;

var clientTwitter = redis.createClient();
var clientInstagram = redis.createClient();
var messageSessionEmmiter = new EventEmitter();

clientTwitter.select(1);
clientInstagram.select(2);

clientTwitter.subscribe('__keyevent@1__:expired');
clientInstagram.subscribe('__keyevent@2__:expired');


clientTwitter.on('message',function(channel, message){
    messageSessionEmmiter.emit('twitterSession',message);
});
clientInstagram.on('message',function(channel, message){
  messageSessionEmmiter.emit('instagramSession', message);
});

module.exports.messageSessionEmmiter = messageSessionEmmiter;
