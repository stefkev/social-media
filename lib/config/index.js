var jsonReader = require("jsonfile");

var filePath = "../config.json";

module.exports.readConf = function(cb){
  jsonReader.readSync(filePath, function(err, obj){
    if (err) throw new Error("Error reading configuration file");
    return cb(obj);
  })
}
