var mongoose = require("mongoose"),
    Schema = mongoose.Schema;
    ObjectId = Schema.ObjectId;

exports.userSchema = new Schema({
    username: String,
    password: String,
    email: String
});
