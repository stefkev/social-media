var mongoose = require("mongoose"),
    Schema = mongoose.Schema;
    ObjectId = Schema.ObjectId;

exports.tweeterSchema = new Schema({
    username: String,
    screen_name: String,
    profile_picture: String,
    text: String
})
