var mongoose = require("mongoose"),
    tweeter = require("./tweeter"),
    instagram = require("./instagram"),
    user = require("./user");

var tweeterModule = mongoose.model('Tweet',tweeter.tweeterSchema);
var instagramModule = mongoose.model('Instagram',instagram.instagramSchema);
var userModule = mongoose.model('User',user.userSchema)

module.exports.tweeterModule = tweeterModule;
module.exports.instagramModule = instagramModule;
module.exports.userModule = userModule;

var conn = mongoose.createConnection("mongodb://localhost/socialMedia");
//bad practice, only use for this version!
conn.once('open',function(){
    userModule.findOne({username: 'ulisses'}, function (err, data) {
      if (err) {
          console.log(err);
      }
      if (data == null || data == undefined) {
          var userInstance = new userModel()
          userInstance.username = 'ulisses';
          userInstance.password = 'garza';
          userInstance.email = 'ulissesgrz@gmail.com';
          userInstance.save(function (err, data) {
              if (err) {
                  throw new Error("Error on creating initial admin");
              }
              console.log("Initial admin created");
          });
      }
      console.log('Initial admin found, no need for new admin creation')
  })
})
