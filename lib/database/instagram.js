var mongoose = require("mongoose"),
    Schema = mongoose.Schema;
    ObjectId = Schema.ObjectId;

exports.instagramSchema = new Schema({
    username: String,
    image: String
});
