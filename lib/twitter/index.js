var twitter = require("twitter"),
    express = require("express"),
    db = require("../database"),
    config = require("../../config.js"),
    EventEmitter = require("eventemitter3").EventEmitter;


var tweetModel = db.tweeterModule;
tweetEmmiter = new EventEmitter();

var twit = new twitter({
    consumer_key: config.twitter.apiKey,
    consumer_secret: config.twitter.apiSecret,
    access_token_key: config.twitter.accessToken,
    access_token_secret: config.twitter.accessTokenSecret
});
twit.stream('statuses/filter', {track: '#' + config.hashtags.twitterHash}, function (stream){
  stream.on('data',function(data){
    var twitDb = new tweetModel();
    twitDb.username = data.user.name;
    twitDb.screen_name = data.user.screen_name;
    twitDb.profile_picture = data.user.profile_image_url;
    twitDb.text = data.text;
    twitDb.save(function(err,data){
      if(err) throw new Error("Error on saving tweets to database");
      tweetEmmiter.emit('tweetMessage',data);
    });
  })
});


module.exports.tweetEmmiter = tweetEmmiter;
