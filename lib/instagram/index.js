var ig = require("instagram-node").instagram(),
    EventEmmiter = require("eventemitter3"),
    express = require("express"),
    util = require("util"),
    config = require("../../config")

var instagramEmmiter = new EventEmmiter();
var instagramRouter = express.Router();
instagramRouter.get('/instagram/callback',function(req, res){
  console.log(req.query);
});
//instagramRouter.get('/instagram/redirect',authorizeUser);
instagramRouter.get('/instagram/notify',function(req, res){

})

ig.use({ client_id: config.instagram.clientID, client_secret: config.instagram.clientSecret});
// ig.add_tag_subscription(obj.hashtag.instagram, obj.hashtag.instagram.notify, verify_key)


ig.subscriptions(function(err, subscriptions, remaining, limit){
  instagramEmmiter.emit('instagramSub',subscriptions);
});

ig.add_tag_subscription(config.hashtags.instagramHash, config.instagram.callbackURL,
 function(err, result, remaining, limit){
  if(err) throw new Error("Error on subscribing on "+config.hashtags.instagramHash);
  console.log(result);
})



module.exports.instagramRouter = instagramRouter;
module.exports.instagramEmmiter = instagramEmmiter;
