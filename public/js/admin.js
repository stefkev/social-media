;(function($) {

    $(document).ready(function() {

        var socket = null;

        if('io' in window) {
            socket = io(window.location.host) //change to your host
        };

        var instagramFeeder = new InstagramFeeder(document.getElementById('instagram-entries'), {
            socket  : socket,

            stampedElementSelector: '.feed-item-stamped',

            columns : 3,
            rows    : 3
        });

        var twitterFeeder = new TwitterFeeder(document.getElementById('twitter-entries'), {
            socket: socket
        });

    });

})(jQuery);


var InstagramFeeder = (function($) {


    function InstagramFeeder(element, settings) {

        this.element = element;

        this.holder = element.parentNode;

        this.settings = settings;

        this.stampedElement = element.querySelector(settings.stampedElementSelector);

        this.socket = this.settings.socket;

        this.stepSize = 0;

        this.init();
    }

    InstagramFeeder.prototype.init = function() {

        this.bind();

        this.setWallSize();


        // TEMP

        if(!this.socket) {
            var that = this;

            setInterval(function() {
                that.addEntry({
                    image    : 'http://scontent-a.cdninstagram.com/hphotos-xap1/t51.2885-15/925299_1471362096442466_425428195_a.jpg',
                    username : 'Test User'
                });
            }, 1500);
        };
    };

    InstagramFeeder.prototype.bind = function() {
        var that = this;


        $(window).on('resize', function() {
            that.setWallSize();
        });

        if(this.socket) {

            this.socket.on('admin-instagram',function(data){

                that.addEntry(data);
            });
        };
    };

    InstagramFeeder.prototype.addEntry = function(data) {
        var entry = this.createEntry(data);

        if(this.element.children.length) {
            if(this.stampedElement) {
                if(this.stampedElement.nextSibling) {
                    this.element.insertBefore(entry, this.stampedElement.nextSibling);
                } else {
                    this.element.appendChild(entry);
                }
            } else {
                this.element.insertBefore(entry, this.element.children[0]);
            }
        } else {
            this.element.appendChild(entry);
        };

        setTimeout(function() {
            entry.className += ' feed-item-appended';
        }, 50);

        this.positionEntries();


    };

    InstagramFeeder.prototype.createEntry = function(data) {
        var temp = document.createElement('div');
        var html = ''
        +   '<div class="feed-item feed-item-1-of-3 instagram-card">'
        +       '<img class="instagram-picture" src="' + data.image + '">'
        +       '<div class="instagram-user">' + data.username + '</div>'
        +   '</div>';

        temp.innerHTML = html;


        return temp.children[0];
    };

    InstagramFeeder.prototype.positionEntries = function() {

        var entries = this.element.children;


        for (var i = 0; i < entries.length; i++) {

            $(entries[i]).css({
                top: parseInt(parseInt(i / this.settings.columns) * this.stepSize),
                left: parseInt(i % this.settings.columns * this.stepSize)
            });
        };
    };


    InstagramFeeder.prototype.setWallSize = function() {
        var wh = $(this.holder).height();
        var ww = $(window).width() / 3 * 2;

        var maxHeight =  wh;
        var maxWidth = ww;

        var maxColWidth = maxWidth / this.settings.columns;
        var maxRowHeight = maxHeight / this.settings.rows;

        if(maxColWidth > maxRowHeight) {
            // fix in width
            $(this.element).css({
                'margin-top' : 0,
                width        : maxRowHeight * this.settings.columns,
                height       : maxRowHeight * this.settings.rows
            });

            this.stepSize = maxRowHeight;
        } else {
            // fix in height
            $(this.element).css({
                'margin-top' : Math.max((wh - maxColWidth * this.settings.rows) / 2 - wh + maxHeight, 0),
                width        : maxColWidth * this.settings.columns,
                height       : maxColWidth * this.settings.rows
            });

            this.stepSize = maxColWidth;
        };

        this.positionEntries();
    };

    return InstagramFeeder;

})(jQuery);



var TwitterFeeder = (function($) {


    function TwitterFeeder(element, settings) {
        this.element = element;

        this.settings = settings;

        this.socket = this.settings.socket;

        this.init();
    }

    TwitterFeeder.prototype.init = function() {

        this.bind();


        // TEMP

        if(!this.socket) {
            var that = this;

            setInterval(function() {
                that.addEntry({
                    profile_picture : 'http://pbs.twimg.com/profile_images/451780385844645888/fvVGKeYP_normal.png',
                    username        : 'Test User',
                    screen_name     : 'ScreenName',
                    text            : '#espectáculo #xcaret #fiesta #charreria #escaramuzas #cancun #quintanaroo #mexico #vive_mexico #loves_mexico #viv... http://t.co/lU2yAaYGL1'

                });
            }, 1500);
        };
    };

    TwitterFeeder.prototype.bind = function() {
        var that = this;


        if(this.socket) {

            this.socket.on('admin-twitter',function(data){

                that.addEntry(data);
            });
        };

    };

    TwitterFeeder.prototype.addEntry = function(data) {
        var entry = this.createEntry(data);

        if(this.element.children.length) {
            this.element.insertBefore(entry, this.element.children[0]);
        } else {
            this.element.appendChild(entry);
        };

        setTimeout(function() {
            entry.className += ' feed-item-appended';
        }, 50);


    };

    TwitterFeeder.prototype.createEntry = function(data) {
        var temp = document.createElement('div');
        var html = ''
        +   '<div class="twitter-item feed-item">'
        +       '<div class="twitter-item-inner">'
        +           '<div class="twitter-item-image">'
        +               '<img src="' + data.profile_picture +  '">'
        +           '</div>'

        +           '<div class="twitter-item-body">'
        +               '<div class="twitter-item-body-head">'

        +                   '<strong class="twitter-name">' + data.username + '</strong> '

        +                   '<span class="twitter-screen-name">'
        +                       '@'

        +                       '<strong>' + data.screen_name + '</strong>'
        +                   '</span>'

        +               '</div>'

        +               '<div class="twitter-item-body-content">'
        +                   '<p>' + data.text + '</p>'
        +               '</div>'
        +           '</div>'
        +       '</div>'
        +    '</div>';

        temp.innerHTML = html;


        return temp.children[0];
    };

    return TwitterFeeder;

})(jQuery);
