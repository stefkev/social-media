var express = require("express"),
    app = express(),
    session = require("cookie-session"),
    bodyParser = require("body-parser"),
    jade = require("jade"),
    io = require('socket.io').listen(app.listen(3000)),
    http = require('http'),
    lib = require("./lib");



app.use(express.static(__dirname + '/public'));
app.use(session({secret: 'ulissesgarza'}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
// app.use(methodOverride());
app.set('view engine', 'jade');
app.use(lib.instagram.instagramRouter);



app.get('/', function (req, res, next) {
    res.render('index');
})



app.get('/admin', function (req, res, next) {
    if (req.session.user_id == null) {
        return next(res.redirect('/'));
    }
    res.render('admin');
})
app.get('/admin/login', function (req, res, next) {
    if (req.session.user_id) {
        return next(res.redirect('/admin'));
    }
    res.render('adminLogin')
    res.end();
})
app.get('/logout', function (req, res, next) {
    req.session = null;
    return next(res.redirect('/'));
})
app.post('/admin/login', function (req, res, next) {
    var post = req.body;
    userModel.findOne({username: post.username, password: post.password}, function (err, data) {
        if (err) {
            return next(res.redirect('/admin/login'));
        }
        if (data == null) {
            return next(res.redirect('/admin/login'));
        } else {
            req.session.user_id = data._id;
            return next(res.redirect('/admin'));
        }
        res.end();
    })
})

//EVENTS FOR ADMIN DELETE TWEETER AND INSTAGRAM
io.on('connection', function (socket) {
    socket.on('deleteTweeter', function (data) {
        clientRedisAdminTwitter.del(data.tweeterId.toString(), function (err) {
            if (err) {
                console.log("something went wrong")
            }
        });
        tweetModel.findByIdAndRemove(data.tweeterId);
    })
    socket.on('deleteInstagram', function (data) {
        clientRedisAdminInstagram.del(data.instagramId.toString(), function (err, data) {
            if (err) {
                console.log("Something went wrong")
            }
        });
        instagramModel.findByIdAndRemove(data.instagramId)
    })
})

//FLUSH DATA ON NODE EXIT
process.on('SIGINT', function () {
    instagramModel.remove(function () {
        console.log("Removed instagram data from DB")
    });
    tweetModel.remove(function () {
        console.log("Removed tweeter data from DB");
    })
    clientRedisAdminInstagram.flushall();
    process.exit();
});
